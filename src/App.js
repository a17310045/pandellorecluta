import React from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { makeStyles } from "@material-ui/core";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Login from "./components/login.js";
import SignUp from "./components/signup.js";
import Tab from "./components/tab.js";

function App() {
	const classes = useStyle();
	return (<Router>
	  <div className="App">
		<nav className="navbar navbar-expand-lg navbar-light fixed-top">
		  <div className="container">
			<div className="collapse navbar-collapse" id="navbarTogglerDemo02">
			  <ul className="navbar-nav ml-auto">
				<li className="nav-item">
				  <Link className="nav-link" to={"/sign-in"}>Iniciar Sesión</Link>
				</li>
				<li className="nav-item">
				  <Link className="nav-link" to={"/sign-up"}>Registrarse</Link>
				</li>
				<li className="nav-item">
				  <Link className="nav-link" to={"/tab"}>Tablero</Link>
				</li>
			  </ul>
			</div>
		  </div>
		</nav>
		
			<Switch>

			  <Route path="/sign-in" component={Login} />
			  <Route path="/sign-up" component={SignUp} />
			  <div className={classes.root}>
				  <div className={classes.container}>
					<Tab/>
					<Tab/>
				</div>
			  </div>
			</Switch>
			
		  
	  </div>
	  
	  </Router>
	  
	);
  }

  const useStyle = makeStyles(theme =>({
    root:{
        minHeight: "100vh",
		
		overflowY: "auto"
    },
	container: {
		display: "flex",
	}
}))
  
  export default App;
