import { Typography, makeStyles } from "@material-ui/core"

const ListTitle = () => {
    const classes = useStyle();
    return(
        <div className={classes.title}>
            <Typography className={classes.titleText}>
                Columna 1
            </Typography>
        </div>
    )
}

const useStyle = makeStyles(theme =>({
    title:{
        display: "flex",
        margin: theme.spacing(1)
    },
    titleText: {
        flexGrow: 1,
        fontSize: "1.3rem",
        fontWeight: "bold"
    }
}))

export default ListTitle