import React, { Component } from "react";
import { CssBaseline, Paper, makeStyles } from "@material-ui/core";
import ListTitle from "./ListTitle";
import Card from "./Card";
import AddCardList from "./AddCardList";
import AddTextCard from "./AddTextCard";

const Tab = () => {
    const classes = useStyle();
    return(
        <Paper className={classes.root}>
          <CssBaseline/>
          <ListTitle/>
          <Card/>
          <Card/>
          <Card/>
          <AddCardList/>
          <AddTextCard/>
          
        </Paper>

        
    )
}
export default Tab

const useStyle = makeStyles(theme =>({
    root:{
        width: "300px",
        background: "#ebecf0",
        margin: theme.spacing(10, 5, 10, 10)
    }
}))