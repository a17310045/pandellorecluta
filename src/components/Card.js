import { Paper, makeStyles } from "@material-ui/core";
import { react } from "react";

const Card = () => {
    const classes = useStyle();
    return(
        <Paper className={classes.card}>
            Tarea 1
        </Paper>
    )
}

const useStyle = makeStyles(theme =>({
    card:{
        padding: theme.spacing(1, 1, 1, 2) ,
        margin: theme.spacing(1)
    }
    
    
}))
export default Card