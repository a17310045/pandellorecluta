import React, { Component } from "react";
import { Button } from 'react-bootstrap';

export default class Login extends Component {
    render() {
        return (
            <div className="auth-wrapper">
            <div className="auth-inner">
            <form>
                <h3>Iniciar Sesión</h3>

                <div className="form-group space-first">
                    <input type="email" className="form-control" placeholder="Correo Electrónico" />
                </div>

                <div className="form-group space-sec">
                    <input type="password" className="form-control" placeholder="Contraseña" />
                </div>

                <div className="text-center d-grid gap-2 space-sec">
                    <Button variant="success" >Iniciar Sesión</Button>{' '}
                    <p className="forgot-password text-center">
                        <a href="#">¿Ya tienes cuenta? Iniciar Sesión</a>
                    </p>
                </div>
                
            </form>
            </div>
            </div>
        );
    }
}