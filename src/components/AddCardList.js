import { Collapse, fade, makeStyles, Paper, Typography } from "@material-ui/core"
import { blue } from "@material-ui/core/colors";
import { useState } from "react"
const AddCardList = () => {
    const [open, setOpen] = useState(false)
    const classes = useStyle();
    return(
        <div className={classes.root}>
            <Collapse in={open}>
            
            </Collapse>
            <Collapse in={ !open}>
            
            </Collapse>
        </div>
    )
}
const useStyle = makeStyles(theme =>({
    root:{
        width: "300px",
        background: "#ebecf0",
        
    },
    addcard:{
        padding: theme.spacing(1,1,1,2),
        margin: theme.spacing(0,1,1,1),
        background: "blue"
    }
}))

export default AddCardList