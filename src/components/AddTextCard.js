import { Input, InputBase, Paper, makeStyles, Button } from "@material-ui/core";
import React, { useState} from "react";

const AddTextCard = () => {
    const [title, setTitle] = useState("")
    const classes = useStyle();
    return(
        <>
        <Paper className={classes.card}>
            <InputBase
                multipline
                value={title}
                onChange={e => setTitle(e.target.value)}
                placeholder="Ingresa un titulo a esta card"
                inputProps = {{className: classes.input}}
            />
                
        </Paper>
        <div className={classes.options}>
            <Button className={classes.btnConfirm} size="large">+ Añadir elemento</Button>
        </div>
        </>
    )
}
const useStyle = makeStyles(theme =>({
    card:{
        width: "280px",
        paddingBottom: theme.spacing(4) ,
        margin: theme.spacing(0, 1, 2, 1)
    },
    input: {
        margin: theme.spacing(1)
    },
    options:{
        flexGrow: 1
    },
    btnConfirm: {
        background: "#005eff",
        color: "white"
    }
    
}))

export default AddTextCard
    